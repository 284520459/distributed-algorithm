/**
 * 
 */
package test.common.distributed.algorithm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import redis.clients.jedis.ShardedJedisPool;

import com.common.distributed.algorithm.RoundRobinWeight.RoundRobinWeightStrategy;
import com.common.distributed.algorithm.blance.IBlanceDistributedAlgorithm;
import com.common.distributed.algorithm.blance.impl.BlanceDistributedAlgorithmImpl;
import com.common.distributed.algorithm.model.AlgorithmModel;
import com.common.distributed.algorithm.redis.mapping.ISearchDistributedIndex;
import com.common.distributed.algorithm.redis.mapping.impl.SearchDistributedIndexImpl;

import junit.framework.TestCase;

/**
 * @author liubing
 *
 */
public class RoundRobinWeightStrategyTest extends TestCase{
	
	private final static Log log = LogFactory.getLog(RoundRobinWeightStrategyTest.class);
	
	public void testRoundRobinWeightStrategy() throws Exception{
		List<AlgorithmModel> algorithmModels=new ArrayList<AlgorithmModel>();
		for(int i=0;i<2;i++){
			AlgorithmModel algorithmModel=new AlgorithmModel();
			algorithmModel.setHost("192.168.1."+i);
			algorithmModel.setWeight(1);
			algorithmModels.add(algorithmModel);
		}
		
		RoundRobinWeightStrategy robinWeightStrategy=new RoundRobinWeightStrategy(algorithmModels);
		for(int i=0;i<40;i++){
			 System.out.println(robinWeightStrategy.getPartitionIdForTopic());  
		}
	}
	
	public void testBlanceDistributedAlgorithm(){
		List<AlgorithmModel> algorithmModels=new ArrayList<AlgorithmModel>();
		int fileCount = 10000;
		int nodeCount = 4;
		for(int i=0;i<nodeCount;i++){
			AlgorithmModel algorithmModel=new AlgorithmModel();
			algorithmModel.setHost("192.168.1."+i);
			algorithmModel.setName("192.168.1."+i);
			algorithmModel.setWeight(1);
			algorithmModels.add(algorithmModel);
		}
		IBlanceDistributedAlgorithm algorithm=new BlanceDistributedAlgorithmImpl(algorithmModels);
		HashMap<String, Integer> nodes = new HashMap<String, Integer>();
		for(int i=1; i<=fileCount; i++){
			
			AlgorithmModel node = algorithm.getAlgorithmModel();
			if(nodes.containsKey(node.getName())){
				nodes.put(node.getName(), (nodes.get(node.getName())+1));
			}else{
				nodes.put(node.getName(), 1);
			}
		}
		System.out.println(nodes);
	}
	
	public void testRedis(){
		 ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring.xml");
		 ShardedJedisPool jedisPool=(ShardedJedisPool) applicationContext.getBean("shardedJedisPool");
		 log.info(jedisPool.getResource().get("liubing"));
	}
	
	public void testSearchDistributedIndexImpl(){
		List<AlgorithmModel> algorithmModels=new ArrayList<AlgorithmModel>();
		int fileCount =400000;
		int nodeCount = 4;
		for(int i=0;i<nodeCount;i++){
			AlgorithmModel algorithmModel=new AlgorithmModel();
			algorithmModel.setHost("192.168.1."+i);
			algorithmModel.setWeight(1);
			algorithmModels.add(algorithmModel);
		}
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring.xml");
		ShardedJedisPool jedisPool=(ShardedJedisPool) applicationContext.getBean("shardedJedisPool");
		ISearchDistributedIndex searchDistributedIndex=new SearchDistributedIndexImpl(algorithmModels);
		searchDistributedIndex.setShardedJedisPool(jedisPool);
		HashMap<String, Integer> nodes = new HashMap<String, Integer>();
		for(int i=1; i<=fileCount; i++){
			String key=searchDistributedIndex.getResultByKey("liubingfileCountTestw1"+i).getName();
			if(nodes.containsKey(key)){
				nodes.put(key, (nodes.get(key)+1));
			}else{
				nodes.put(key, 1);
			}
		}
		log.info(nodes);
	}
	
	public void testTest(){
		for(int i=0;i<1000;i++){
			System.out.println(i);
		}
	}
}
